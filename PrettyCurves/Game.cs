﻿using OpenTK;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrettyCurves
{
	class Game : GameWindow
	{
		List<Line> lines;
		List<Spline> splines;
		Vector2 currentPos;
		float currentDir, currentRadius, currentSpeed, currentRadiusChange;
		bool currentRight;
		Random rand;
		Texture2D background;
		Vector2 lastMousePos;
		int stepsMouseMove;

		public Game()
			: base(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width - 100, 
			System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - 100,
			new OpenTK.Graphics.GraphicsMode(32, 0, 0, 24), "Splines", GameWindowFlags.Default)
		{
			//GL.Enable(EnableCap.Blend);
			//GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
			GL.Disable(EnableCap.CullFace);

			background = ContentPipe.LoadTexture("back1.jpg", false);
			lastMousePos = new Vector2(this.Mouse.X, this.Mouse.Y);
			stepsMouseMove = 0;
			this.CursorVisible = true;

			rand = new Random();
			lines = new List<Line>();
			splines = new List<Spline>();
			
			CreateNewSpline(new Vector2(400, 300), (float)(rand.NextDouble() * MathHelper.Pi * 2), true);
		}
		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);

			this.Exit();
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);

			if (lines.Count > 6000)
			{
				lines = new List<Line>();
				splines = new List<Spline>();
				CreateNewSpline(new Vector2(400, 300), (float)(rand.NextDouble() * MathHelper.Pi * 2), true);
			}
			AddToSpline();// AddToSpline(); AddToSpline();

			//if ((new Vector2(this.Mouse.X, this.Mouse.Y) - lastMousePos).LengthSquared > 25)
			//{
			//	//stepsMouseMove++;
			//	//if (stepsMouseMove >= 3)
			//	//	this.Exit();
			//}
			//else
			//{
			//	//stepsMouseMove = 0;
			//}
			lastMousePos = new Vector2(this.Mouse.X, this.Mouse.Y);
		}
		private void AddToSpline()
		{
			Vector2 add = new Vector2(
				(float)Math.Cos(currentDir) * currentSpeed, 
				(float)Math.Sin(currentDir) * currentSpeed);

			Line newLine = new Line(currentPos, currentPos + add);
			Vector2 intersect;
			bool hit = false;
			for (int i = 0; i < lines.Count - 1; i++)
			{
				if (Line.Intersects(lines[i], newLine, out intersect))
				{
					newLine.p2 = intersect;
					hit = true;
					break;
				}
			}
			lines.Add(newLine);
			splines[splines.Count - 1].AddPoint(newLine.p2);
			if (hit)
			{
				Vector2 startPos, directionVec;
				bool turnRight;
				FindSplinePos(out startPos, out directionVec, out turnRight);
				CreateNewSpline(startPos, (float)Math.Atan2(directionVec.Y, directionVec.X), turnRight);
			}
			else
			{
				currentPos += add;

				
				currentRadiusChange += 0.003f;
				this.currentRadius += currentRadiusChange;
				if (currentRadius < 3f)
					currentRadius = 3f;
				float dirChange = (float)(currentSpeed / (2 * Math.PI * currentRadius)) * (float)Math.PI * 2f;
				this.currentDir += (currentRight ? dirChange : -dirChange);
				if (currentDir > MathHelper.Pi * 2)
					currentDir -= MathHelper.Pi * 2;
				if (currentDir < 0)
					currentDir += MathHelper.Pi * 2;
			}
		}
		private void FindSplinePos(out Vector2 start, out Vector2 direction, out bool turnRight)
		{
			start = new Vector2(-1, -1);
			direction = Vector2.Zero;
			turnRight = true;
			while (start.X < 0 || start.X > this.Width || start.Y < 0 || start.Y > this.Height)
			{
				int r = rand.Next(0, lines.Count);
				start = (lines[r].p1 + lines[r].p2) / 2f;
				if (rand.Next(0, 2) > 0)
				{
					direction = (lines[r].p2 - lines[r].p1).PerpendicularLeft;
					turnRight = true;
				}
				else
				{
					direction = (lines[r].p2 - lines[r].p1).PerpendicularRight;
					turnRight = false;
				}
			}
		}
		private void CreateNewSpline(Vector2 startPos, float direction, bool turnRight)
		{
			this.currentDir = direction;
			this.currentRadius = 100f;
			this.currentPos = startPos;
			this.currentSpeed = (float)rand.NextDouble() * 4f + 1f;
			this.currentRadiusChange = -1f;
			this.currentRight = turnRight;
			splines.Add(new Spline(currentPos));
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			GL.Viewport(new Rectangle(0, 0, this.Width, this.Height));
			GL.ClearColor(Color.Black);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.MatrixMode(MatrixMode.Projection);
			Matrix4 mat = Matrix4.CreateTranslation(-this.Width / 2f, -this.Height / 2f, 0) *
				Matrix4.CreateScale(2f / this.Width, -2f / this.Height, 1f);
			GL.LoadMatrix(ref mat);

			GL.Enable(EnableCap.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, background.Id);
			GL.Begin(PrimitiveType.Quads);
			GL.Color4(Color.Gray);
			GL.TexCoord2(0, 0); GL.Vertex2(0, 0);
			GL.TexCoord2(1, 0); GL.Vertex2(this.Width, 0);
			GL.TexCoord2(1, 1); GL.Vertex2(this.Width, this.Height);
			GL.TexCoord2(0, 1); GL.Vertex2(0, this.Height);
			GL.End();

			GL.Disable(EnableCap.Texture2D);
			for (int i = 0; i < splines.Count; i++)
			{
				splines[i].Draw(Color.Blue, 3f);
			}
			for (int i = 0; i < splines.Count; i++)
			{
				splines[i].Draw(Color.Cyan, 1f);
			}
			//GL.Begin(PrimitiveType.Lines);
			//GL.Color3(Color.FromArgb(128, Color.White));
			//for (int i = 0; i < lines.Count; i++)
			//{
			//	GL.Vertex2(lines[i].p1); GL.Vertex2(lines[i].p2);
			//}
			//GL.End();
			//GL.LineWidth(1f);
			//GL.Begin(PrimitiveType.Lines);
			//GL.Color3(Color.FromArgb(200, Color.LightCyan));
			//for (int i = 0; i < lines.Count; i++)
			//{
			//	GL.Vertex2(lines[i].p1); GL.Vertex2(lines[i].p2);
			//}
			//GL.End();

			GL.Flush();
			this.SwapBuffers();
		}

		private void DrawCircle(Vector2 center, float radius, Color color, int numSteps, bool outline = false, float outlineWidth = 2f, Color? outlineColor = null)
		{
			GL.Begin(PrimitiveType.Triangles);
			GL.Color4(color);
			for (int i = 0; i < numSteps; i++)
			{
				float ang1 = (float)(i) / numSteps * MathHelper.Pi * 2;
				float ang2 = (float)(i + 1) / numSteps * MathHelper.Pi * 2;
				GL.Vertex2(center);
				GL.Vertex2(
					(float)Math.Cos(ang1) * radius + center.X,
					(float)Math.Sin(ang1) * radius + center.Y);
				GL.Vertex2(
					(float)Math.Cos(ang2) * radius + center.X,
					(float)Math.Sin(ang2) * radius + center.Y);
			}
			GL.End();

			if (outline)
			{
				GL.LineWidth(outlineWidth);
				GL.Begin(PrimitiveType.LineLoop);
				GL.Color4((outlineColor.HasValue) ? outlineColor.Value : Color.White);
				for (int i = 0; i < numSteps; i++)
				{
					float ang1 = ((float)i / numSteps) * MathHelper.Pi * 2;

					GL.Vertex2(
						(float)Math.Cos(ang1) * radius + center.X,
						(float)Math.Sin(ang1) * radius + center.Y);
				}
				GL.End();
			}
		}
	}
}
