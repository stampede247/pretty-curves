﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace PrettyCurves
{
	class Spline
	{
		private List<Vector2> points;

		public Spline(Vector2 startpos)
		{
			points = new List<Vector2>();
			points.Add(startpos);
		}

		public void AddPoint(Vector2 pos)
		{
			this.points.Add(pos);
		}

		public void Draw(Color color, float width)
		{
			if (points.Count > 1)
			{
				GL.Begin(PrimitiveType.TriangleStrip);
				GL.Color4(color);
				for (int i = 0; i < points.Count; i++)
				{
					Vector2 add;
					if (i == 0)
						add = (points[1] - points[0]).PerpendicularRight;
					else
						add = (points[i] - points[i - 1]).PerpendicularRight;
					add.Normalize();
					add *= width;

					GL.Vertex2(points[i] + add);
					GL.Vertex2(points[i] - add);
				}
				GL.End();
			}
		}
	}
}
