﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrettyCurves
{
	struct Line
	{
		public Vector2 p1, p2;

		public Line(Vector2 p1, Vector2 p2)
		{
			this.p1 = p1;
			this.p2 = p2;
		}

		public static bool Intersects(Line l1, Line l2)
		{
			Vector2 intersect;
			return Intersects(l1, l2, out intersect);
		}
		public static bool Intersects(Line l1, Line l2, out Vector2 intersection)
		{
			//http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
			Vector2 p = l1.p1; //line1 start point
			Vector2 r = l1.p2 - l1.p1; //line1 direction vector
			Vector2 q = l2.p1; //line2 start point
			Vector2 s = l2.p2 - l2.p1; //line2 direction vector

			if (Cross(r, s) == 0)
			{
				//The lines are collinear or parallel
				//We do not support collinear intersection because
				//we would not know what point to return
				//Therefore we treat it as no intersection
				intersection = Vector2.Zero;
				return false;
			}

			float t = Cross(q - p, s) / Cross(r, s);
			float u = Cross(q - p, r) / Cross(r, s);

			if (0 <= t && t <= 1 && 0 <= u && u <= 1)
			{
				intersection = p + t * r; //or q + u * s
				return true;
			}

			intersection = Vector2.Zero;
			return false;
		}

		private static float Cross(Vector2 p1, Vector2 p2)
		{
			return p1.X * p2.Y - p1.Y * p2.X;
		}
	}
}
