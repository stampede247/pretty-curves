﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrettyCurves
{
	class Program
	{
		static void Main(string[] args)
		{
			Game game = new Game();
			
			game.Run(1.0 / 60.0);
		}
	}
}
